({
    doInit : function(component, event, helper) {
        console.log("Retrieved quotelineitemid = " + component.get("v.quotelineitemid"));
        var action = component.get("c.getQuoteLineItem");
        action.setParams({
            "quotelineItemId" : component.get("v.quotelineitemid")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log("retrieved result = " + result);
                // Set isproducctoprice based on the retrieved product's QuantityUnitOfMeasure value
                var quoteLine = component.get("v.result");
				component.set("v.qlirecord", quoteLine);
            }else{
             	console.log("unsuccessful");
                // provide a toaster with the error message then close the component
                this.handleErrors(response.getError());
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    },
    onClickCalculatePSA: function(component, event, helper){
        
    },
    handleErrors: function(errors){
        var errorMessage = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            errorMessage = errors[0].message;
        } 
        // Configure error toast
        let toastParams = {
            title: "Error with Server Operations",
            message: errorMessage, 
            type: "error"
        };
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },    
    
})