({
    doInit : function(component, event, helper) {
        console.log("Retrieved recordId = " + component.get("v.recordId"));
        var action = component.get("c.getQuoteLineItems");
        action.setParams({
            "quoteId" : component.get("v.recordId"),
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.quotelines", result);
                console.log("retrieved result = " + result);
                // Set isproducctoprice based on the retrieved product's QuantityUnitOfMeasure value
                var quoteLines = component.get("v.quotelines");
                var quoteLinesLength = quoteLines.length;
                if(quoteLinesLength === 0){
                    component.set("v.isproducttoprice", false);
                    return;
                }else{
                    component.set("v.isproducttoprice", true);
                    for(var i = 0; i < quoteLinesLength; i++){
                        var quoteItem = quoteLines[i];
                        console.log("Quote Product Name = " + quoteItem.Product2.Name + ", UnitMeasure = " + quoteItem.Product2.QuantityUnitOfMeasure);
                    }                    
                }
            }else{
             	console.log("unsuccessful");
                // provide a toaster with the error message then close the component
                this.handleErrors(response.getError());
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);

    },
	onTestAction : function(component, event, helper) {
		
	},
	onClickCalculatePrice : function(component, event, helper) {
        // Set the right pricing flag so that the related pricing component opens up in the modal
        var buttonName = event.getSource().get("v.name"); // return myButton
        console.log("Retrieved button Name = " + buttonName);
        helper.clearAll(component, event);
        var splitName = buttonName.split("_");
        var splitNameLength = splitName.length;
        for(var i = 0; i < splitNameLength; i++){
            console.log("i = " + i + ",  related string = " + splitName[i]);
        }
        component.set("v.quotelineitemid", splitName[1]);
        if(buttonName.includes("Patent Annuities") && buttonName.includes("custom")){
            component.set("v.ispsa", true);
        }
        if(buttonName.includes("Diams") && buttonName.includes("custom")){
            component.set("v.isdiams", true);
        }
	},
   	onClickCloseModal: function(component, event, helper) {
      // Set all pricing flags to false -> closes the modal
      helper.clearAll(component, event);
   	},
   	onClickSaveResult: function(component, event, helper) {
      // If Needed ?? - Save the result back to the QuoteLineItem which invoked the flow
      helper.clearAll(component, event);
   	},

    handleErrors: function(errors){
        var errorMessage = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            errorMessage = errors[0].message;
        } 
        // Configure error toast
        let toastParams = {
            title: "Error with Server Operations",
            message: errorMessage, 
            type: "error"
        };
        // Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },    
})