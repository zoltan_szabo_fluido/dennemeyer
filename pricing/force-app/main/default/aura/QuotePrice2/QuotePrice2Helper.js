({
    clearAll : function(component, event){
        // reset all product configuration flags to false
        // additional products may be added once the configuration gets bigger
        component.set("v.ispsa", false);
        component.set("v.isdiams", false);
        
    },
})