({
    doInit : function(component, event, helper) {
        var DiamsFlowTry = component.find("DiamsPricingId");
        var inputVariables = [
            {
                name:"quoteId",
                type:"String",
                value:component.get("v.quoteid")
            },
            {
                name:"quoteLineItemId",
                type:"String",
                value:component.get("v.quotelineitemid")
            },
            
        ];
         
        DiamsFlowTry.startFlow("Diams_Flow_Try", inputVariables);
    },
     handleStatusChange : function(component, event, helper) {
        if(event.getParam("status") === "FINISHED") {
         /*var outputVariables = event.getParam("outputVariables");
          var outputVar;
          for(var i = 0; i < outputVariables.length; i++) {
             outputVar = outputVariables[i];
             if(outputVar.name === "contactId") {
                var urlEvent = $A.get("e.force:navigateToSObject");
                urlEvent.setParams({
                   "recordId": outputVar.value,
                   "isredirect": "true"
                });
                urlEvent.fire();
             }
          }*/
          $A.get('e.force:refreshView').fire();
 
       }
    }
})