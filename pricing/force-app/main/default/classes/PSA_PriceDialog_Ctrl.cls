/*
 * Controller for the PSA_PriceDialog Lightning Component
 */
public class PSA_PriceDialog_Ctrl {

    /*
     * Define the return values of the PSA callout in a wrapper class
     */
    public class PSA_Wrapper{
        public Double feeCalculated {get; set;}
        public String suggestedInterval {get; set;}
        public String penaltyRate {get; set;}
        
        public PSA_Wrapper(Double feeCalculated, String suggestedInterval, String penaltyRate){
            this.feeCalculated = feeCalculated;
            this.suggestedInterval = suggestedInterval;
            this.penaltyRate = penaltyRate;
        }
    }
    
	/* 
	 * Retrieve the PSA Quoteline item
	 */
    @AuraEnabled
    public static QuoteLineItem getQuoteLineItem(Id quotelineItemId){
        QuoteLineItem retqli = [
            select Id, QuoteId, isPricingCompleted__c, isPricingRequired__c, 
            	Product2Id, Product2.Name
            from QuoteLineItem
            where Id =: quotelineItemId
            limit 1
        ];
        return retqli;
    }

    /* 
     * Save a modified quoteLineItem after calculation
     */
    @AuraEnabled
    public static void saveQuoteLineItem(Id quotelineItemId, Double monthlyFee, 
                                         String suggestedInterval, Double penaltyRate ){
		System.debug('Saving the PSA QuoteLineItem');
    }
    
    /*
     * Callout to PSA portal
     */
    @AuraEnabled
    public static boolean calculatePSA( 
        	String region, 
        	String monthInAdvanceInstructions, 
            Double optimalDeposit,
   			String iCurrency,
        	String paymentTerm,
        	String paymentTermMode,
        	String frequencyOfInvoices,
        	String caseDueIn,
        	String caseDueBy,
        	Integer activeCases){
		System.debug('Saving the PSA QuoteLineItem');
    		
            return true;
            
  	}
    
	    
}