@isTest
public class Opportunity_TH_Test {

    @testSetup static void setup(){
        TestDataFactory.createAccounts();
        TestDataFactory.createProducts();
        TestDataFactory.createPricebooks();
        TestDataFactory.createPricebookEntries();
        TestDataFactory.createOpportunities();
        TestDataFactory.createOpportunityLineItems();
    }
    
    @isTest public static void afterUpdate_Test(){
        List<Opportunity> retopps = new List<Opportunity>([
            select Id, Name, StageName, accountId, account.Name
            from Opportunity
        ]);
        System.debug('In afterUpdate_Test: retopps size = ' + retopps.size());
        System.debug('Printing all opportunities');
        for(Opportunity tmpopp : retopps){
            System.debug('Next opportunity Name = ' + tmpopp.Name + ', Account = ' + tmpopp.Account.Name + ', Stage = ' + tmpopp.StageName);
        }
        Integer oppCounter = 0;
        for(Opportunity opp : retopps){
            opp.StageName = 'On Contract';
            oppCounter++;
        }
        update retopps;
        System.debug('Number of oppportunities = ' + retopps.size());
        // Check if Contracts were creasted
        List<Contract> retcontrs = new List<Contract>([
            select Id from Contract
        ]);
        System.debug('Number of contracts created = ' + retcontrs.size());
        System.assertEquals(oppCounter, retcontrs.size());
    }
    
}