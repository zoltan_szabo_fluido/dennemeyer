@isTest
public class TestDataFactory {
	// Test data variables
	// Account
	static List<Account> accounts = null;
	static Map<Id, Account> accountsMap = null;
    static Integer accountsNum = 0;
    static List<String> accountNames;
	static List<String> accountStreets;
	static List<String> accountPostalCodes;
    static List<String> accountCities;
    static List<String> accountCountries;    

    // Product
	static List<Product2> products = null;
    static Integer productsNum = 0;
    
    // Contact
	static List<Contact> contacts = null;
    static Integer contactsNum = 0;
    
	// Pricebook
	// Standard pricebook plus the ones which are defined in TestDataSet by their name. All are active.
	static Id stdPricebookId = Test.getStandardPricebookId();
	static List<Pricebook2> pricebooks = null;
	static Map<Id, Pricebook2> pricebooksMap = null;			// Id of Pricebooks and the related Pricebook
    static Map<Id, List<Id>> pricebookToProductsMap = null;	// Id of Pricebook and list of IDs of the related Products
    static Integer pricebooksNum = 0;

    // Pricebook Entry
	static List<PricebookEntry> pbEntries = null;
	static Map<Id, PricebookEntry> pbToPbEntries = null; 		// Pricebook to list of related PB Entries
    static Integer pbEntriesNum = 0;
    
	// Opportunity
	static List<Opportunity> opportunities = null;
    static Map<Id, Opportunity> opportunitiesMap = null;
    static Integer opportunitiesNum = 0;

	// Opportunity LineItem
	static List<OpportunityLineItem> oppLineItems = null;
    static Integer oppLineItemsNum = 0;

	// Quote
	static List<Quote> quotes = null;
    static Integer quotesNum = 0;
    
	// QuoteLineItem
	static List<QuoteLineItem> quoteLineItems = null;
    static Integer quoteLineItemsNum = 0;

    // Contract
	static List<Contract> contracts = null;
    static Integer contractsNum = 0;
    
	// Lead
	static List<Lead> leads = null;
    static Integer leadsNum = 0;

	// Country
/*	static List<pw_cc__CountryObject__c> countries = null;
    static Integer countriesNum = 0;
*/    
    
    @isTest public static void createAccounts() {
		Integer i;
        Integer recSize;
		accountNames = TestDataSet.getAccountNames();
		accountStreets = TestDataSet.getAccountStreets();
		accountCities = TestDataSet.getAccountCities();
		accountCountries = TestDataSet.getAccountCountries();
		accountPostalCodes = TestDataSet.getAccountPostalCodes();

        accounts = new List<Account>();
        recSize = accountNames.size();
		// Add accounts
		for(i = 0; i < recSize; i++) {
			accounts.add(new Account(
                Name = accountNames[i],
			 	BillingStreet = accountStreets[i],
			 	BillingCity = accountCities[i],
			 	BillingCountry = accountCountries[i],
               	BillingPostalCode = accountPostalCodes[i]
			));
		}
		insert accounts;
        accountsMap = new Map<Id, Account>(accounts);
        accountsNum = accounts.size();
		System.debug('createAccounts ENDS. Inserted records size = ' + accountsNum);
		return;    	
	}
    
    @isTest public static void createContacts(){
        
    }
    
    @isTest public static void createProducts(){
        products = TestDataSet.getProducts();
        insert products;
        productsNum = products.size();
		System.debug('createProducts ENDS. Inserted records size = ' + productsNum);
		return;        
    }    

    @isTest public static void createPricebooks(){
        List<String> pricebookNames = TestDataSet.getPricebookNames();
        pricebooks = new List<Pricebook2>();
        Integer recSize = pricebookNames.size();
        for(Integer i = 0; i < recSize; i++){
            Pricebook2 pb = new Pricebook2(
            	Name = pricebookNames.get(i),
            	IsActive = true);
            pricebooks.add(pb);
        }
        insert pricebooks;
        pricebooksNum = pricebooks.size();
		System.debug('createPricebooks ENDS. Inserted records size = ' + pricebooksNum);
    }
    
    @isTest public static void createPriceBookEntries(){
        if(pricebooks == null || products == null)
            return;
        // Create pricebookentries for the standard pricebook
        List<PricebookEntry> pbStdEntries = new List<PricebookEntry>();
        Integer priceVal = 1000;
        for(Product2 pr : products){
            PricebookEntry pbe = new PricebookEntry(
                product2Id = pr.Id,
                Pricebook2Id = stdPricebookId,
                IsActive = true,
                UnitPrice = priceVal);
            pbStdEntries.add(pbe);
            priceVal += 1000;
        }
        insert pbStdEntries;       
        
        // Create pricebook entries for all other pricebooks
        pbEntries = new List<PricebookEntry>();
        for(Pricebook2 pb: pricebooks){
        	priceVal = 1000;
            for(Product2 pr : products){
                PricebookEntry pbe = new PricebookEntry(
                	product2Id = pr.Id,
                	Pricebook2Id = pb.Id,
                	UseStandardPrice = false,
                	IsActive = true,
                	UnitPrice = priceVal);
                pbEntries.add(pbe);
                priceVal += 1000;
            }
        }
        insert pbEntries;
        pbEntriesNum = pbEntries.size();
        // re-retrieve pricebooks with the newly create pb entrie also included
        pricebooks = new List<Pricebook2>([
            select Id, Name, IsActive,
            (select Id, Name, Pricebook2Id, Pricebook2.Name, Product2.Name, UnitPrice from PricebookEntries)
            from Pricebook2
        ]);
        // Populate pricebookToProductsMap map
        pricebookToProductsMap = new Map<Id, List<Id>>();
        for(Pricebook2 pb2 : pricebooks){
            List<Id> prIds = new List<Id>();
            pricebookToProductsMap.put(pb2.Id, prIds);
            for(PricebookEntry pbe : pb2.PricebookEntries){
                pricebookToProductsMap.get(pb2.Id).add(pbe.Product2Id);
            }
            
        }
        pricebooksMap = new Map<Id, Pricebook2>(pricebooks);
		System.debug('createPricbookEntries ENDS. Inserted records size = ' + pbEntriesNum);
    }

    @isTest public static void createOpportunities(){
        Integer recSize = TestDataSet.OPPORTUNITY_NUMBER;
        Integer pbCounter = 0;
		Integer pbSize;
        Integer acctSize;
        if(accounts == null || pricebooks == null){
            return;
        }
       	pbSize = pricebooks.size();
        acctSize = accounts.size();
		opportunities = new List<Opportunity>();
        for(Integer i = 0; i < acctSize; i++){
            for(Integer j = 0; j < recSize; j++){
		        Opportunity opp = new Opportunity(
                    AccountId = accounts.get(i).Id,
                    name = accounts.get(i).Name + '_Opp_' + i, 
                    Pricebook2id = pricebooks.get(pbCounter).id,
                    StageName = 'Open', 
                    CloseDate = Date.today().addMonths(1), 
                    CurrencyIsoCode = 'EUR');
                pbCounter = pbCounter == ( pbSize - 1) ?
                    0 : pbCounter + 1;
                opportunities.add(opp);
            }
        }
        insert opportunities; 
        opportunitiesMap = new Map<Id, Opportunity>(opportunities);
        opportunitiesNum = opportunities.size();
		System.debug('createOpportunities ENDS. Inserted records size = ' + opportunitiesNum);
        
    }

    @isTest public static void createOpportunityLineItems(){
        if(opportunities == null || products == null){
            return;
        }
        oppLineItems = new List<OpportunityLineItem>();
        for(Opportunity opp: opportunities){
            for(Integer i = 0; i < TestDataSet.PRODUCT_TO_OPP_NUMBER; i++){
                Pricebook2 tmppbe = pricebooksMap.get(opp.Pricebook2Id); 
                List<PricebookEntry> pbe = tmppbe.PricebookEntries;
                Integer pbeCounter = 0;
                Integer pbeNumber = pbe.size();
                OpportunityLineItem oppli = new OpportunityLineItem(
                    Quantity = 1,
                    OpportunityId = opp.Id,
                    PricebookEntryId = pbe.get(pbeCounter).Id
                	);
                oppLineItems.add(oppli);
                pbeCounter = pbeCounter == (pbeNumber - 1) ? 0 : pbeCounter + 1;
            }
        }
        insert oppLineItems;
        oppLineItemsNum = oppLineItems.size();
		System.debug('createOpportunityLineItems ENDS. Inserted records size = ' + oppLineItemsNum);        
    }
    
    @isTest public static void createContracts(){
        // The method creates Contract without Opportunity StageName = 'On Contract' setting
		List<Id> oppIds = new List<Id>();
        List<OpportunityLineItem> opplis = new List<OpportunityLineItem>();
        
        if(opportunities == null || oppLineItems == null)
            return;

        for(Opportunity opp : opportunities){
            // get all opportunity Ids
            oppIds.add(opp.Id);
        }
		opplis = new List<OpportunityLineItem>([
            select id, Name, Opportunity.Id, product2.productGroup__c 
            from OpportunityLineItem
            where Opportunity.Id in: oppIds
        ]);
        // Organize the oppProducts around the Opportunities as oppId -> list of Opp Product
        Map<Id, List<OpportunityLineItem>> oppToOppliMap = new Map<Id, List<OpportunityLineItem>>();
        for(Opportunity opp : opportunities){
            oppToOppliMap.put(opp.Id, new List<OpportunityLineItem>());
            for(OpportunityLineItem oppli : opplis){
                if(oppli.OpportunityId == opp.Id){
                    oppToOppliMap.get(opp.Id).add(oppli);
                }
            }
        }

        contracts = new List<Contract>();
        for(Opportunity opp : opportunities){

            // Check all related Opportunity Products. If any related Product Group is new, 
            // ... add a new Contract record
            List<String> productGroups = new List<String>();
            List<OpportunityLineItem> relatedOppProducts = oppToOppliMap.get(opp.Id);
            for(OpportunityLineItem oppli : relatedOppProducts){
                if(!productGroups.contains(oppli.Product2.productGroup__c)){
                	productGroups.add(oppli.Product2.productGroup__c);
                    // Create a new Contract record
                    Contract newContract = new Contract(
                    	AccountId = opp.AccountId,
                        Status = 'Draft',
                        BillingStreet = opp.Account.BillingStreet,
                        BillingCity = opp.Account.BillingCity,
                        BillingState = opp.Account.BillingState,
                        BillingCountry = opp.Account.BillingCountry,
                        BillingPostalCode = opp.Account.BillingPostalCode,
                        CurrencyIsoCode = opp.CurrencyIsoCode,
						Name = oppli.Product2.productGroup__c + '_' + opp.Name,
                        Opportunity__c = opp.Id,
                        PriceBook2Id = opp.Pricebook2Id,
                        StartDate = opp.CloseDate + 14,
                        productGroup__c = oppli.Product2.productGroup__c                    
                    );
                    contracts.add(newContract);
            	}
            }
    	}
        insert contracts;
        contractsNum = contracts.size();
		System.debug('createContracts ENDS. Inserted records size = ' + contractsNum);        
            
	}
    
    @isTest public static void createQuotes(){
        if(opportunities == null || oppLineItems == null)
            return;
        quotes = new List<Quote>();
        for(Opportunity opp : opportunities){
            // Create the required number of quotes. The last quote is synced.
            for(Integer i = 0; i < TestDataSet.QUOTE_TO_OPP_NUMBER; i++){
                Quote qte = new Quote(
                    Name = opp.Name + '_' + i,
                    OpportunityId = opp.Id
            	);
                quotes.add(qte);
              }  
       	}
        insert quotes;
        
        // re-query opportunities and sync the first quote from the ones related to the opportunity        
        opportunities = new List<Opportunity>([
            select Id, Name, SyncedQuoteId, AccountId, Pricebook2Id, StageName, CloseDate, CurrencyIsoCode,
            	(select id, OpportunityId, Opportunity.Name, Opportunity.Pricebook2Id from quotes)
            from Opportunity
            
        ]);
        for(Opportunity opp : opportunities){
            if(opp.SyncedQuoteId == null && opp.quotes != null){
                opp.SyncedQuoteId = opp.quotes.get(0).Id;
            }
        }
        update opportunities;
        quotes = new List<Quote>([
            select Id, AccountId, IsSyncing, Pricebook2Id, OpportunityId, Opportunity.Pricebook2Id
            from Quote
        ]);
        quotesNum = quotes.size();
		System.debug('createQuotes ENDS. Inserted records size = ' + quotesNum);                
    }

    @isTest public static void createQuoteLineItems(){
        System.debug('createQuoteLineItem STARTS');
        if(opportunities == null || quotes == null)
            return;
        quoteLineItems = new List<QuoteLineItem>();
        for(Quote qte : quotes){
            // Create the required number of quote line items for quotes which are synced.
            Pricebook2 actPricebook = pricebooksMap.get(opportunitiesMap.get(qte.OpportunityId).pricebook2Id);
            List<PricebookEntry> pbEntryList = actPricebook.PricebookEntries;
            Integer pbEntryListNum = pbEntryList.size();
            Integer pbEntryIndex = 0;
                        
            List<Id> productsList = pricebookToProductsMap.get(actPricebook.Id);
            Integer productsListNum = productsList.size();
            if(qte.IsSyncing != true)
                continue;
            for(Integer i = 0; i < TestDataSet.QLI_TO_QUOTE_NUMBER; i++){
                QuoteLineItem qli = new QuoteLineItem(
                    Quantity = 1,
                    UnitPrice = 1,
                    QuoteId = qte.Id,
                    PricebookEntryId = pbEntryList.get(pbEntryIndex).Id                    
            	);
                pbEntryIndex = (pbEntryIndex == pbEntryListNum - 1) ? 
                    0 : pbEntryIndex + 1; 
                quoteLineItems.add(qli);
              }  
       	}
        try{
        	insert quoteLineItems;
        }catch (Exception e){
            System.debug('Error when saving Test Quoteline items. Error = ' + e.getMessage());
        }
        quoteLineItems = new List<QuoteLineItem>([
            select Id, Product2Id, Product2.Name, PricebookEntryId, PricebookEntry.Pricebook2Id, PricebookEntry.Pricebook2.Name,
            	UnitPrice, Quantity, Product2.productGroup__c
            from QuoteLineItem
        ]);
        quoteLineItemsNum = quoteLineItems.size();
        // re-query quotes too
        quotes = new List<Quote>([
            select Id, QuoteNumber, IsSyncing, numberOfProductsPriced__c, numberOfProductsToPrice__c,
            	AccountId, OpportunityId, Opportunity.Pricebook2Id, 
            	(select id, Product2Id, Product2.Name, UnitPrice, Quantity, Product2.productGroup__c from QuoteLineItems)
            from Quote            
        ]);
		System.debug('createQuoteLineItems ENDS. Inserted records size = ' + quoteLineItemsNum);
		/*System.debug('Printing all QuoteLineItems');
        for(QuoteLineItem qli : quoteLineItems){
            System.debug('Next qli Product = ' + qli.Product2.Name + ', Pricebook = ' + qli.PricebookEntry.Pricebook2.Name + ', UnitPrice = ' + qli.UnitPrice);
        }*/        
    }
    
    @isTest public static void createLeads(){
		leads = TestDataSet.getLeads();	
        insert leads;
        leadsNum = leads.size();
    }

/*    @isTest public static void createCountries(){
		countries = TestDataSet.getCountries();	
        insert countries;
        countriesNum = countries.size();
    }
*/
    
}