@IsTest
public class QuotePrice_Ctrl_Test {

    @testSetup static void setup(){
        TestDataFactory.createAccounts();
        TestDataFactory.createProducts();
        TestDataFactory.createPricebooks();
        TestDataFactory.createPricebookEntries();
        TestDataFactory.createOpportunities();
        TestDataFactory.createOpportunityLineItems();
        TestDataFactory.createQuotes();
    }
    
    @isTest public static void getQuoteLineItems_Test(){
        List<Quote> retQuotes = new List<Quote>([
            select Id, IsSyncing 
            from Quote
            where IsSyncing = true
        ]);
        System.debug('Number of Quote records retrieved = ' + retQuotes.size());
        
        
    }
    
    
    
}