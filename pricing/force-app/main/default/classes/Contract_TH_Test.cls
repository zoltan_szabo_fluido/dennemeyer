@isTest
public class Contract_TH_Test {

    @testSetup static void setup(){
        TestDataFactory.createAccounts();
        TestDataFactory.createProducts();
        TestDataFactory.createPricebooks();
        TestDataFactory.createPricebookEntries();
        TestDataFactory.createOpportunities();
        TestDataFactory.createOpportunityLineItems();
        TestDataFactory.createContracts();
    }
    
    @isTest public static void afterUpdate_Test(){
        System.debug('Contract_TH_Test.afterUpdate_Test STARTS');
		// Test if Contract records have been created
		// All existing Contracts are supposed to be in Draft status
		Test.startTest();
        List<Contract> retContracts = new List<Contract>([
            select Id, Status 
            from Contract
            where Status = 'Draft'
        ]);
        System.debug('Number of retrieved contracts = ' + retContracts.size());
        Integer toActivateNumber = retContracts.size() < 5 ? retContracts.size() : 5;
        System.debug('toActivateNumber = ' + toActivateNumber);
        List<Contract> toUpdateContracts = new List<Contract>();
        for(Integer i = 0; i < toActivateNumber; i++){
            toUpdateContracts.add(retContracts[i]);
        }
        for(Integer i = 0; i < toActivateNumber; i++){
            toUpdateContracts[i].Status = 'Activated';
        }
        System.debug('Updatig toUpdateContracts STARTS now. Size of the list = ' + toUpdateContracts.size());
        update toUpdateContracts;
        Test.stopTest();
		List<Order> retOrders = new List<Order>([
            select Id
            from Order
        ]);
		System.assertEquals(toActivateNumber, retOrders.size());        
		        
    }
}