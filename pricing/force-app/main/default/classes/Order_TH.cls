/*
 * If Orders are created and are in Draft, add all related Order Item records
 * Assign the Order Items from the Opportunity Line Items if 
 * the Opportunity Line Item belongs to the same Product Group of Dennemeyer
 */

public class Order_TH extends TriggerHandler{ 

    private List<Order> newrecs;
    private List<Order> oldrecs;
    Integer recSize;
    Profile sysAdminProfile = null;
    User usr = null;
    

    public Order_TH() {
        System.debug('Order_TH constructor STARTS');
        this.newrecs = (List<Order>) Trigger.New;
        this.oldrecs = (List<Order>) Trigger.Old;
        if(Trigger.isDelete == false){
	        this.recSize = newrecs.size();        
        }
        System.debug('Order_TH constructor ENDS');
    }


    public override void afterInsert() {
        System.debug('Order_TH.afterInsert STARTS');
        Map<Id, Opportunity> opps = null;
		List<OpportunityLineItem> opplis = null;
        Set<Id> oppIdSet = new Set<Id>();
        List<OrderItem> toSaveOrderItems = null;
        for(Order act: newrecs) {
            if(act.OpportunityId != null){
                oppIdSet.add(act.OpportunityId);
            }
        }
        opps = new Map<Id, Opportunity>([
            select Id, name, Amount,
            	(select id, Quantity, UnitPrice, PricebookEntryId, Product2Id, Product2.Name, Product2.productGroup__c 
             	from OpportunityLineItems)
            from Opportunity
            where Id in: oppIdSet
        ]);
        List<OrderItem> toSaveOItems = new List<OrderItem>();
        for(Order act : newrecs){
            if(act.productGroup__c == null ||
               order.OpportunityId == null){
                   continue;
               }
            Opportunity opp = opps.get(act.OpportunityId);
            opplis = opp.OpportunityLineItems;
            for(OpportunityLineItem oppli : opplis){
                if(oppli.Product2.productGroup__c == act.productGroup__c){
            		OrderItem oi = new OrderItem(
                    	Product2 = oppli.Product2,
                        OrderId = act.Id,
                        Quantity = oppli.Quantity,
                        UnitPrice = oppli.UnitPrice,
                        PricebookEntryId = oppli.PricebookEntryId                    
                    );
                    toSaveOItems.add(oi);
                }                
            }
        }
        System.debug('Saving toSaveOrderItems NOW');
        insert toSaveOItems;
        System.debug('Order_TH.afterInsert ENDS');
        return;
    }


}