/*
 * Executes simpler pricce with mulitiplication on the following Priducts:
 * - Octimine
 * It shall be no effect on any other product
 */

public class QuoteLineItem_TH extends TriggerHandler{

    private List<QuoteLineItem> newrecs;
    private List<QuoteLineItem> oldrecs;
    Integer recSize;

	// Quote related variables
  	Set<Id> quoteIds = new Set<Id>();
    Map<Id, Quote> retQuotesMap = null;
    
	// Product related variables
    Set<Id> productIds = null;
    List<Product2> retProducts = null;
    Map<Id, Product2> retProductsMap = null;

	// Octimine custom metadata price information related variables
    List<Octimine_Price__mdt> oPrices = null;
	Integer oPricesSize = 0;

    // PricebookEntries related variables
    Set<Id> pricebookIds = null;						// Pricebook Ids
    Set<Id> priceBookEntryIds = null;					// PricebookEntry Ids
    List<PricebookEntry> retPBEs = null;				// Retrieved PricebookEntry records
    Map<Id, PricebookEntry> retPBEsMap = null;				// Retrieved PricebookEntry records
    Map<Id, List<PricebookEntry>> productToPBEMap = null;	// Products to PricebookEntries of any currency
    
    
    public QuoteLineItem_TH() {
        System.debug('QuoteLineItem_TH constructor STARTS');
        this.newrecs = (List<QuoteLineItem>) Trigger.New;
        this.oldrecs = (List<QuoteLineItem>) Trigger.Old;
        if(Trigger.isDelete == true){
            return;
        }
        this.recSize = newrecs.size();
        getQuotes();				// Retrieve quotes
		getOctiminePricing();    	// Retrieve Octimine Price custom metadata
        getPricebookEntries();
        getProducts();        		// Retrieve all product related information
		populateProductToPBE();		// Create the Product Id to PBE records map
        System.debug('QuoteLineItem_TH constructor ENDS');
    }

    public override void beforeInsert() {
        System.debug('QuoteLineItem_TH.beforeInsert STARTS');
        calculateOctimine();
        System.debug('QuoteLineItem_TH.beforeInsert ENDS');
        return;
    }

    public override void beforeUpdate() {
        System.debug('QuoteLineItem_TH.beforeUpdate STARTS');
		calculateOctimine();
        System.debug('QuoteLineItem_TH.beforeUpdate ENDS');
        return;
    }

    private void calculateOctimine(){
        // Get all product's Unit price from the right pricebook and currency code
		System.debug('calculateOctimine STARTS');
        List<Integer> indexValues = new List<Integer>();
        // To store the indexes whose Quote Line Item contains Octimine Software        
        for(Integer i = 0; i < recSize; i++){
            QuoteLineItem actual = newrecs.get(i); 
            System.debug('Actual QuoteLineItem record = ' + actual);
            Id actualProductId = RetPBEsMap.get(actual.pricebookEntryId).Product2Id;
            if(retProductsMap.get(actualProductId).Family == 'Software' &&
               retProductsMap.get(actualProductId).productGroup__c == 'Octimine'&& 
               retProductsMap.get(actualProductId).Name.contains('Octimine')){
                   
                   indexValues.add(i);
               }            
        }
        for(Integer j : indexValues){
            // Loop over QuiteLine Items which had Octimine quotes
            QuoteLineItem actual = newrecs.get(j);
            Boolean isFound = false;

            // Modify the qli UnitPrice: Find the first custom metadata records which has the right user number
            //String retCurrency = retQuotesMap.get(actual.quoteId).CurrencyIsoCode;
            Id actualProductId = retPBEsMap.get(actual.pricebookEntryId).Product2Id;
            Double retUnitPrice = getUnitPrice(actualProductId, retQuotesMap.get(actual.QuoteId).Pricebook2Id);
            for(Integer i = 0; i < oPrices.size() && !isFound; i++){
                if(oPrices.get(i).numberOfUsers__c >= actual.Quantity){
                     	actual.UnitPrice = (Double) (oPrices.get(i).singleUserMultiplier__c * retUnitPrice / 100).round();
                      	isFound = true;
                   }
            }
            if(!isFound){
                actual.addError('ERROR in Octimine Pricing: Contact your System administrator');
                return;
            }
        }        
		System.debug('calculateOctimine ENDS');
        return;
        
    }
    
    private Double getUnitPrice(Id iProductId, Id iPricebookId){
        for(Id prodId : productToPBEMap.keySet()){
            List<PricebookEntry> relList = productToPBEMap.get(prodId);
        }
        Double returnValue = -1.0;
        Boolean isUnitFound = false;
        List<PricebookEntry> possiblePBEs = productToPBEMap.get(iProductId);
    
        for(PricebookEntry pbe : possiblePBEs){
            if(pbe.Pricebook2Id == iPricebookId){
                return pbe.UnitPrice;
            }
        }
        return returnValue;        
    }
    
    private void getQuotes(){
        for(QuoteLineItem qli : newrecs){
            quoteIds.add(qli.QuoteId);
        }
        retQuotesMap = new Map<Id, Quote>([
            select Id, Name, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode, Opportunity.Name
            from Quote
            where Id =: quoteIds
        ]);
    }      

    private void getPricebookEntries(){
        pricebookEntryIds = new Set<Id>();
        pricebookIds = new Set<Id>();
        for(QuoteLineItem qle : newrecs){
            pricebookEntryIds.add(qle.PricebookEntryId);
        }
        retPBEs = new List<PricebookEntry>([
            select Product2Id, PriceBook2Id, UnitPrice, Product2.Name, Pricebook2.Name
            from PricebookEntry
            where Id in:pricebookEntryIds
        ]);
        System.debug('Printing retrieved Pricebook entries');
        for(PricebookEntry pbee : retPBEs){
            System.debug('Pricebook Name = ' + pbee.Pricebook2.Name + ', Product name = ' + pbee.Product2.Name);
        }
        retPBEsMap = new Map<Id, PricebookEntry>(retPBEs);        
    }
    
    private void getProducts(){
        productIds = new Set<Id>();
        for(PricebookEntry pbe : retPBEs){
            productIds.add(pbe.Product2Id);
        }
		retProducts = new List<Product2>([
            select Id, Name, productGroup__c, Family, isRecurring__c, costCenter__c
            from Product2
            where Id in: productIds            
        ]);
        retProductsMap = new Map<Id, Product2>(retProducts);
    }
    
       
    private void getOctiminePricing(){
        if(oPrices == null){
                oPrices = new List<Octimine_Price__mdt >([
                select Id, Label, numberOfUsers__c, singleUserMultiplier__c
                from Octimine_Price__mdt
                order by numberOfUsers__c ASC
            ]);
            oPricesSize = oPrices.size();            
        }
    }

    private void populateProductToPBE(){
    	// Create the Product id to PBE records map
        productToPBEMap = new Map<Id, List<PricebookEntry>>();
        for(PricebookEntry pbe : retPBEs){
            if(productToPBEMap.keySet().contains(pbe.Product2Id)){
                productToPBEMap.get(pbe.Product2Id).add(pbe);
            }else{
                List<PricebookEntry> tmpList = new List<PricebookEntry>();
                tmpList.add(pbe);
                productToPBEMap.put(pbe.Product2Id, tmpList);
            }
        }
    }
}