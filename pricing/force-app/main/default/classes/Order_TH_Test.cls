@isTest
public class Order_TH_Test {

    @testSetup static void setup(){
        TestDataFactory.createAccounts();
        TestDataFactory.createProducts();
        TestDataFactory.createPricebooks();
        TestDataFactory.createPricebookEntries();
        TestDataFactory.createOpportunities();
        TestDataFactory.createOpportunityLineItems();        
    }
    
    @isTest public static void afterInsert_Test(){
        List<Order> retOrders = new List<Order>([
            select Id, Name, contractId, Status
            from Order
        ]);
    }

}