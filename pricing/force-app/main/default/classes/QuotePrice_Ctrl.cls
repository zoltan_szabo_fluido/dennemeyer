/*
 * Retrieve QuoteLineItem records related to the input quote 
 * whose isPricingRequired__c = true
 */

public class QuotePrice_Ctrl {
    
    @AuraEnabled
    public static List<QuoteLineItem> getQuoteLineItems(Id quoteId){
        List<QuoteLineItem> retQuoteLineItems = new List<QuoteLineitem>([
            select Id, QuoteId, Product2.Name, Product2.QuantityUnitOfMeasure,
            	isPricingCompleted__c, isPricingRequired__c  
            from QuoteLineItem
            where QuoteId =: quoteId
            and isPricingRequired__c = true
        ]);
        System.debug('getQuoteLineItems return. Return items = ' + retQuoteLineItems);        
        return retQuoteLineItems;
    }

}