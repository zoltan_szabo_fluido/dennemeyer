@isTest
public class PSA_PriceDialog_Ctrl_Test {

    @testSetup static void setup(){
        TestDataFactory.createAccounts();
        TestDataFactory.createProducts();
        TestDataFactory.createPricebooks();
        TestDataFactory.createPricebookEntries();
        TestDataFactory.createOpportunities();
        TestDataFactory.createOpportunityLineItems();
        TestDataFactory.createQuotes();
        TestDataFactory.createQuoteLineItems();
    }

    @isTest static void getQuoteLineItem_Test(){
        List<QuoteLineItem> retqlis = new List<QuoteLineItem>([
            select Id, QuoteId
            from QuoteLineItem
            where Quote.IsSyncing = true
        ]);
		QuoteLineItem qli1 = retqlis.get(0);
        QuoteLineItem qli2 = PSA_PriceDialog_Ctrl.getQuoteLineItem(qli1.Id);
        System.assertEquals(qli1.Id, qli2.Id);
        
        
    }
    
}