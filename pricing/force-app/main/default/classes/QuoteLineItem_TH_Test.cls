@isTest
public class QuoteLineItem_TH_Test {

    @testSetup static void setup(){
        TestDataFactory.createAccounts();
        TestDataFactory.createProducts();
        TestDataFactory.createPricebooks();
        TestDataFactory.createPricebookEntries();
        TestDataFactory.createOpportunities();
        TestDataFactory.createOpportunityLineItems();
        TestDataFactory.createQuotes();
        TestDataFactory.createQuoteLineItems();
    }
    
    @isTest static void beforeInsert_Test(){
		// The test assumes that the custom metadata values are unchanged.
		// The quantity of the new quote line item is set for 1 -> only the unit price will defined the validity
		// of the System.assertEquals method.

        // Pick up a quote
		Quote qte = [select Id, Name, OpportunityId, CurrencyIsoCode, Pricebook2Id, Opportunity.Name from Quote limit 1];
        System.debug('Quote retrieved = ' + qte);
        Product2 octiProd = [
            select Id, Name, isRecurring__c, productGroup__c
        	from Product2
        	where Name = 'Octimine Search Subscription'
        ];
        PricebookEntry pbe = [
            select Id, Product2Id, Pricebook2Id
            from PricebookEntry
            where product2Id =: octiProd.Id
            and Pricebook2Id =: qte.Pricebook2Id
            limit 1
        ];
        QuoteLineItem qli = new QuoteLineItem(
            Quantity = 1,
            UnitPrice = 1,
            QuoteId = qte.Id,
            PricebookEntryId = pbe.Id                    
        );
        insert qli;
        
        // retrieve the same quoteline item
        QuoteLineItem qli2 = [
            	select Id, QuoteId, UnitPrice
                from QuoteLineItem 
                where Id =: qli.Id
                limit 1
        ];
        System.debug('qli2 UnitPrice = ' + qli2.UnitPrice);
        System.assertEquals(2000.0, qli2.UnitPrice);		
    }

    @isTest static void beforeUpdate_Test(){
        
    }
    
}