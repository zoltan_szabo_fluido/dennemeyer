/*
* It contains test data for Apex tests
*/
public class TestDataSet {

    // Defaults to be used when test data is created
	public static Integer CONTACT_NUMBER = 3; 			// Number of contacts created for each account
    public static Integer OPPORTUNITY_NUMBER = 2; 		// Number of opportunities created for each account
    public static Integer PRODUCT_TO_OPP_NUMBER = 2; 	// Number of products added to each opportunity
    public static Integer QUOTE_TO_OPP_NUMBER = 2;		// Number of quotes related to Opportunity
    public static Integer QLI_TO_QUOTE_NUMBER = 6;		// Number of quotelineitems related to every quote
    

    public static List<String> getAccountNames(){
        List<String> accountNames = new List<String>{
            'Allianz GmbH'/*, 
            'Tesla, Inc', 
            'ABB Switzerland', 
            'Chanel', 
            'Philips'*/
        };
        return accountNames;
    }

    public static List<String> getAccountStreets(){
        List<String> accountStreet = new List<String>{
            'Prinz-Eugen-Str. 2', 
            'Prinz-Eugen-Str. 2', 
            'Prinz-Eugen-Str. 2', 
            'Prinz-Eugen-Str. 2', 
            'Prinz-Eugen-Str. 2'
        };
        return accountStreet;
    }

    public static List<String> getAccountPostalCodes(){
        List<String> accountPostalCode = new List<String>{
            '80804',
            '80804',
            '80804',
            '80804',
            '80804'
        };
        return accountPostalCode;
    }
    
    public static List<String> getAccountCities(){
        List<String> accountCity = new List<String>{
            'Munich',
            'Munich',
            'Munich',
            'Munich',
            'Munich'
        };
        return accountCity;
    }

    public static List<String> getAccountCountries(){
        List<String> accountCountry = new List<String>{
            'Germany', 
            'Germany', 
            'Germany', 
            'Germany', 
            'Germany'
        };
        return accountCountry;
    }

/*    public static List<String> getFirstNames(){
        List<String> firstNames = new List<String>{
            'Jürgen', 'Sara', 'Daniel', 'Thomas', 'Katharina',
            'Andre', 'Roland', 'Stefan', 'Arndt', 'Ingo',
            'Eva', 'Alexandra', 'Tamara', 'Josef', 'Britta',
            'Lina', 'Klaus', 'Pierre', 'Stephan', 'Karl'
        };
        return firstNames;
    }

    public static List<String> getLastNames(){
        List<String> lastNames = new List<String>{
            'Schumacher', 'Münchner', 'Kloos', 'Vogt', 'Hamburger',
            'Schuster', 'Taylor', 'Wiener', 'Roth', 'Smart',
            'Schmitt', 'Schneider', 'Weber', 'Fischer', 'Becker',
            'Klein', 'Salzburger', 'Münchner', 'Berliner', 'Nürnberger'
        };
        return lastNames;
    }*/

    public static List<Product2> getProducts(){
        List<Product2> products = new List<Product2>{
            new Product2(Name = 'Patent Annuities_0', QuantityUnitOfMeasure  = 'custom', productGroup__c = 'PSA', 
                         isRecurring__c = true, Family = 'Service'),
            new Product2(Name = 'Octimine Search Subscription', QuantityUnitOfMeasure  = 'custom', productGroup__c = 'Octimine', 
                         isRecurring__c = true, Family = 'Software'),
            new Product2(Name = 'Octimine Search Subscription 2', QuantityUnitOfMeasure  = 'custom', productGroup__c = 'Octimine', 
                         isRecurring__c = true, Family = 'Software'),
                
            new Product2(Name = 'Diams_SW', QuantityUnitOfMeasure  = 'each', productGroup__c = 'DIAMS', 
                         isRecurring__c = true, Family = 'Software'),
            new Product2(Name = 'Patent Annuities_1', QuantityUnitOfMeasure  = 'custom', productGroup__c = 'PSA', 
                         isRecurring__c = false, Family = 'Service'),

            new Product2(Name = 'Octimine_Service', QuantityUnitOfMeasure  = 'man_day', productGroup__c = 'Octimine', 
                         isRecurring__c = false, Family = 'Service'),
            new Product2(Name = 'Diams_Service', QuantityUnitOfMeasure  = 'man_day', productGroup__c = 'DIAMS', 
                         isRecurring__c = false, Family = 'Service'),
            new Product2(Name = 'Patent Annuities_2', QuantityUnitOfMeasure  = 'man_day', productGroup__c = 'PSA', 
                         isRecurring__c = true, Family = 'Service')
        };
        return products;
    }
    
    public static List<String> getPricebookNames(){
        List<String> pricebookNames = new List<String>{
            'EMEA', 
            'Americas', 
            'APAC' 
		};
        return pricebookNames;
    }

    public static List<Lead> getLeads(){
        List<Lead> leads = new List<Lead>{
			new Lead( LastName = 'TestLead_1', Company = 'TestCompany_1', Country = 'Germany', PostalCode = '81666',
                     LeadSource = 'External Events', Source_Details__c = 'Trade Show', Email = 'test@tes.comm'),
			new Lead( LastName = 'TestLead_2', Company = 'TestCompany_2', Country = 'Germany', PostalCode = '81666',
                     LeadSource = 'External Events', Source_Details__c = 'Trade Show', Email = 'test@tes.comm'),
			new Lead( LastName = 'TestLead_3', Company = 'TestCompany_3', Country = 'Germany', PostalCode = '81666',
                     LeadSource = 'External Events', Source_Details__c = 'Trade Show', Email = 'test@tes.comm')
		};
        return leads;
    }

/*    public static List<pw_cc__CountryObject__c> getCountries(){
        List<pw_cc__CountryObject__c> countries = new List<pw_cc__CountryObject__c>{
			new pw_cc__CountryObject__c(Name = 'Romania', pw_cc__IsoCode_2__c = 'RO', pw_cc__IsoCode_3__c = 'ROU'),
			new pw_cc__CountryObject__c(Name = 'Hungary', pw_cc__IsoCode_2__c = 'HU', pw_cc__IsoCode_3__c = 'HUN'),
			new pw_cc__CountryObject__c(Name = 'Germany', pw_cc__IsoCode_2__c = 'DE', pw_cc__IsoCode_3__c = 'DEU'),
			new pw_cc__CountryObject__c(Name = 'France', pw_cc__IsoCode_2__c = 'FR', pw_cc__IsoCode_3__c = 'FRA')
		};
        return countries;
    }
*/    
}