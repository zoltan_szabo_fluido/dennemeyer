/*
 * When an Opportunity is closed Won, create related Contract records.
 * The number of related Contract records depends on the number of Product Groups 
 * listed among the selected opportunity products.
 */


public class Opportunity_TH extends TriggerHandler{ 

    private List<Opportunity> newrecs;
    private List<Opportunity> oldrecs;
    Integer recSize;
    List<Profile> profiles = null;
    User usr = null;
    
    List<OpportunityLineItem> retoplis;

    public Opportunity_TH() {
        System.debug('Opportunity_TH constructor STARTS');
        this.newrecs = (List<Opportunity>) Trigger.New;
        this.oldrecs = (List<Opportunity>) Trigger.Old;
        if(Trigger.isDelete == false){
            this.recSize = newrecs.size();            
        }
        // Temporary code until complete user test is done:
        // Only Sysadmin shall run this TH code
        profiles = new List<Profile>([
            select Id, Name 
            from Profile
            where Name = 'System Administrator'
        ]);
        if(profiles == null || profiles.size() != 1)
            return;
        Id userId = UserInfo.getUserId();
        usr = [
            select Id, Name, ProfileId
            from User
            where Id =: userId
        ];        
        System.debug('Opportunity_TH constructor ENDS');
    }


    public override void afterUpdate() {
        System.debug('Opportunity_TH.afterUpdate STARTS');

        if(profiles == null || profiles.size() != 1 || usr.ProfileId != profiles[0].Id){
            return;
        }
		
        List<Contract> toSaveContracts = new List<Contract>();
		List<Id> oppIds = new List<Id>();
        List<OpportunityLineItem> opplis = new List<OpportunityLineItem>();
        
        for(Opportunity opp : newrecs){
            if(opp.StageName != 'On Contract')
                continue;
            // get all opportunity Ids
            oppIds.add(opp.Id);
        }
		opplis = new List<OpportunityLineItem>([
            select id, Name, Opportunity.Id, product2.productGroup__c 
            from OpportunityLineItem
            where Opportunity.Id in: oppIds
        ]);
        // Organize the oppProducts around the Opportunities as oppId -> list of Opp Product
        Map<Id, List<OpportunityLineItem>> oppToOppliMap = new Map<Id, List<OpportunityLineItem>>();
        for(Integer i = 0; i < recSize; i++){
            Opportunity newOpp = newrecs[i];
            Opportunity oldOpp = oldrecs[i];

            if(newOpp.StageName != 'On Contract'){
                continue;
            }
            oppToOppliMap.put(newOpp.Id, new List<OpportunityLineItem>());
            for(OpportunityLineItem oppli : opplis){
                if(oppli.OpportunityId == newOpp.Id){
                    oppToOppliMap.get(newOpp.Id).add(oppli);
                }
            }
        }
        //for each Opportunity, create potential contract records, 
        //... as many as different ProductGroup items the products contain
        for(Integer i = 0; i < recSize; i++){
            Opportunity newOpp = newrecs[i];
            Opportunity oldOpp = oldrecs[i];

            if(newOpp.StageName != 'On Contract' ||
              (newOpp.StageName == 'On Contract' && oldOpp.StageName == 'On Contract')){
                  continue;
        	}
            // Check all related Opportunity Products. If any related Product Group is new, 
            // ... add a new Contract record
            List<String> productGroups = new List<String>();
            List<OpportunityLineItem> relatedOppProducts = oppToOppliMap.get(newOpp.Id);
            //for(OpportunityLineItem oppli : relatedOppProducts){
            //    System.debug('Next opp product = ' + oppli);
            //}
            for(OpportunityLineItem oppli : relatedOppProducts){
                if(!productGroups.contains(oppli.Product2.productGroup__c)){
                	productGroups.add(oppli.Product2.productGroup__c);
                    // Create a new Contract record
                    Contract newContract = new Contract(
                    	AccountId = newOpp.AccountId,
                        Status = 'Draft',
                        BillingStreet = newOpp.Account.BillingStreet,
                        BillingCity = newOpp.Account.BillingCity,
                        BillingState = newOpp.Account.BillingState,
                        BillingCountry = newOpp.Account.BillingCountry,
                        BillingPostalCode = newOpp.Account.BillingPostalCode,
                        CurrencyIsoCode = newOpp.CurrencyIsoCode,
						Name = oppli.Product2.productGroup__c + '_' + newOpp.Name,
                        Opportunity__c = newOpp.Id,
                        PriceBook2Id = newOpp.Pricebook2Id,
                        StartDate = newOpp.CloseDate + 14,
                        productGroup__c = oppli.Product2.productGroup__c                    
                    );
                    toSaveContracts.add(newContract);
            	}
            }
    	}
        insert toSaveContracts;
        System.debug('Opportunity_TH.afterUpdate ENDS');

    }

}