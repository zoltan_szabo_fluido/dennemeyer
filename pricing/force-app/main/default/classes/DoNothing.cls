/*
 * Invocable method to be used by Process Builder Flows
 * Serves to exit without any activity at the beginning of a PB flow...
 * ...if the running user's isBypassAutomation__c checkbox is set to true
 */

public class DoNothing {
    
    @InvocableMethod (label='Do Nothing')
    public static void doNothing(){
        return;
    }

}