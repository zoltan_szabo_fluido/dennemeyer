/*
 * When an Opportunity is closed Won, create related Contract records.
 * The number of related Contract records depends on the number of Product Groups 
 * listed among the selected opportunity products.
 */


public class Contract_TH extends TriggerHandler{ 

    private List<Contract> newrecs;
    private List<Contract> oldrecs;
    Integer recSize;
    

    public Contract_TH() {
        System.debug('Contract_TH constructor STARTS');
        this.newrecs = (List<Contract>) Trigger.New;
        this.oldrecs = (List<Contract>) Trigger.Old;
        if(Trigger.isDelete == false){
	        this.recSize = newrecs.size();        
        }
        System.debug('Contract_TH constructor ENDS');
    }

	/* 
	 * Create related Order and Order Product records from the parent Opportunity record
	 * when Contract.Status changes to Activated
	 * One Contract has one child Order.
	 * Order products contain products which have the same product group than the Contract's Product Group.
	 */
    public override void afterUpdate() {
		Set<Id> oppIDSet = new Set<Id>();
        List<OpportunityLineItem> opplis = new List<OpportunityLineItem>();
        Map<Id, Opportunity> opps;
        Map<Id, Contract> contractMap = new Map<Id, Contract>();
        contractMap.putAll(newrecs);
     	// Retrieve all related Opportunities and Opportunity line items
        for(Integer i = 0; i < recSize; i++){
			Contract newc = newrecs.get(i);
            Contract oldc = oldrecs.get(i);
            if(newc.Status != oldc.Status &&
               newC.Status == 'Activated'){
            	oppIdSet.add(newc.opportunity__c);
        	}
        }
        opps = new Map<Id, Opportunity>([
            select Id, Name
            from Opportunity
            where Id in:oppIdSet
               ]);
        opplis = new List<OpportunityLineItem>([
            select Id, Name, OpportunityId, Product2Id, Product2.Name, Product2.productGroup__c, 
            TotalPrice, unitOfMeasure__c, Quantity, UnitPrice
            from OpportunityLineItem
            where OpportunityId in: oppIdSet
        ]);
        // Assign each Opportunity product to the right contract id
        Map<Id, List<OpportunityLineItem>> contractToOppliMap = new Map<Id, List<OpportunityLineItem>>();
        for(Integer i = 0; i < recSize; i++){
			Contract newc = newrecs.get(i);
            Contract oldc = oldrecs.get(i);
            if(newc.Status != 'Activated' ||
               (newc.Status == oldc.Status)){
                   continue;
          	}
            contractToOppliMap.put(newc.Id, new List<OpportunityLineItem>());
            // collect all opportunity line items related to this new, activated contract
            for(OpportunityLineItem oppli : opplis){
                if(newc.opportunity__c == oppli.OpportunityId){
                 	contractToOppliMap.get(newc.Id).add(oppli);   
                }                
            }
        }
        // for each contract in ContractToOppliMap, create an order and related order products
        List<Order> toSaveOrders = new List<Order>();
        for(Id cntrId : contractToOppliMap.keySet()){
            Contract cntr = contractMap.get(cntrId);
            Order newOrder = new Order(
            	ContractId = cntrId,
                Status = 'Draft',
                OpportunityId = cntr.opportunity__c,
                EffectiveDate = cntr.StartDate,
                productGroup__c = cntr.productGroup__c
                
            );
			toSaveOrders.add(newOrder);   
        }
        insert toSaveOrders;
    }

}